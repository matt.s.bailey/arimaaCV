clear variables
close all
% Open movie file.
movieObj = VideoReader('Calibration.wmv');
nFrames = movieObj.NumberOfFrames;
MOMENTS = zeros(2, 7, 3); % Empty, Rabbit, Cat, Dog, Horse, Camel, Elephant 1=WHITE CIRCLE 2=BLACK CIRCLE
fileID = fopen('calibration.calib','w');
%fileID = fopen('HuCalibration.calib','w');
fprintf('%d\n', nFrames);
for iFrame=5:5:nFrames
    fprintf('%d\n', iFrame);
    I = read(movieObj,iFrame);
    checkerHistogram = zeros(8, 8, 2);
    %figure(1), imshow(I), title(sprintf('Frame %d', iFrame));
    [corners, nMatches, avgErr] = findCheckerBoard(I);
    line([corners(1, 1) corners(2, 1)], [corners(1, 2) corners(2, 2)], 'LineWidth', 3)
    line([corners(1, 1) corners(4, 1)], [corners(1, 2) corners(4, 2)], 'LineWidth', 3)
    line([corners(2, 1) corners(3, 1)], [corners(2, 2) corners(3, 2)], 'LineWidth', 3)
    line([corners(3, 1) corners(4, 1)], [corners(3, 2) corners(4, 2)], 'LineWidth', 3)
    fixed = [0 0; 400 0; 400 400; 0 400];
    tform = fitgeotrans(corners, fixed, 'projective');
    I2 = imwarp(I, tform, 'OutputView', imref2d([400, 400], [0, 400], [0, 400]));
    I3 = rgb2gray(I2);
    i = 1;
    for k=1:2
        p = 1;
        for j=1:50:350
            if i == 1
                I5 = I3(1:50, j:j+49);
            else
                I5 = I3(51:100, j:j+49);
            end
            I5 = im2bw(I5, 0.5);
            %if (I5(25,25) == 0)
            if (I5(27,43) == 1)
                I5(:,1:11) = 1;
                I5(1:11,:) = 1;
                I5(:,39:50) = 1;
                I5(39:50,:) = 1;
            else
                I5(:,1:11) = 0;
                I5(1:11,:) = 0;
                I5(:,39:50) = 0;
                I5(39:50,:) = 0;
            end
            %figure(5), imshow(I5)
            %invariant = Hu_Moments(I5);
            invariant(1) = moment(I5, 1, 0);
            invariant(2) = moment(I5, 0, 1);
            invariant(3) = moment(I5, 1, 1);
            MOMENTS(k, p, 1) = invariant(1) + MOMENTS(k, p, 1);
            MOMENTS(k, p, 2) = invariant(2) + MOMENTS(k, p, 2);
            MOMENTS(k, p, 3) = invariant(3) + MOMENTS(k, p, 3);
            if i == 1
                i = 2;
            else
                i = 1;
            end
            p = p + 1;
        end
    end
end
%Set black square to zero rather than NaN
MOMENTS(2, 7, :) = 0.0;
MOMENTS(1, 1, 1)
MOMENTS(1, 1, 2)
MOMENTS(1, 1, 3)
%Get the averages of the measured moments
for i=1:2
    for j=1:7
        MOMENTS(i, j, 1) = MOMENTS(i, j, 1) / 19;
        MOMENTS(i, j, 2) = MOMENTS(i, j, 2) / 19;
        MOMENTS(i, j, 3) = MOMENTS(i, j, 3) / 19;
    end
end
%write to file
for i=1:2
    for k=1:3
        fprintf(fileID, '%.15f ', MOMENTS(i, 7, k));
    end
    
    for j=1:6
        for k=1:3
            fprintf(fileID, '%.15f ', MOMENTS(i, j, k));
        end
    end
    fprintf(fileID, '\n');
end
fclose(fileID);
